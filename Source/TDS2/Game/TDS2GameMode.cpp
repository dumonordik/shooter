// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS2GameMode.h"
#include "TDS2PlayerController.h"
#include "TDS2/Character/TDS2Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS2GameMode::ATDS2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Blueprint'/Game/Blueprints/Character/BP_Character'"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}