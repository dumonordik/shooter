// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS2/FuncLibrary/Types.h"
#include "TDS2/FuncLibrary/Types.h"
#include "TDS2/Game/WorldItemDefault.h"
#include "TDS2Character.generated.h"

UCLASS(Blueprintable)
class ATDS2Character : public ACharacter
{
	GENERATED_BODY()
protected:
 virtual void BeginPlay() override; 
public:
	ATDS2Character();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;
	
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	class UArrowComponent* ArrowComponent;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
 // Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor");
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor");
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
 // Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EmovementState MovementState=EmovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	//Weapon	
    	AWeaponDefault* CurrentWeapon = nullptr;
    
    //for demo 
    	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
    	TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;
	
	UDecalComponent* CurrentCursor = nullptr;

	
	
 // Inputs
	float AxisX =0.0f;
	float AxisY =0.0f;
	
	
 // Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);
 // Func	
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EmovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon();

	
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();
};



